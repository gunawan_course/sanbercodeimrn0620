/**
 * Nama : Gunawan
 * Email : gunawan.prasetyo@protonmail.com
 * Tugas 5
 */

/**************************
 * Tugas Array
 * Soal 1
 **************************/

function range(startNum, finishNum) {
    let numbers = [];
    if (startNum == null || finishNum == null) {
        return -1;
    }
    
    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            numbers.push(i);
        }
    } else {
        for (let i = startNum; i <= finishNum; i++) {
            numbers.push(i);        
        }
    }

    return numbers;
}

console.log(range(10,1));
// console.log(range(1, 10));


/**************************
 * Tugas Array
 * Soal 2
 **************************/

function rangeWithStep(startNum, finishNum, step = 1) {
    let numbers = [];
    if (startNum == null || finishNum == null) {
        return -1;
    }
    
    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            numbers.push(i);
        }
    } else {
        for (let i = startNum; i <= finishNum; i += step) {
            numbers.push(i);        
        }
    }

    return numbers;
}

console.log(rangeWithStep(1,10,2));

/**************************
 * Tugas Array
 * Soal 3
 **************************/

function sum(numbers = []) {
    let total = 0;
    for (let i = 0; i < numbers.length; i++) {
        total = total + numbers[i];
    }

    return total;
}

console.log( sum( rangeWithStep(5, 50, 2) ) )

/**************************
 * Tugas Array
 * Soal 4
 **************************/

function dataHandling(data) {
    let list = ``;
    for (let i = 0; i < data.length; i++) {
        list += `Nomor ID : ${data[i][0]} \r\nNama Lengkap : ${data[i][1]} \r\nTTL : ${data[i][2]}, ${data[i][3]} \r\nHobby : ${data[i][4]} \r\n\r\n`;
    }

    return list;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

console.log(dataHandling(input));

/**************************
 * Tugas Array
 * Soal 5
 **************************/

function balikKata(words = "") {
    let reverseWord = '';
    for (let i = words.length -1; i >= 0; i--) {
        reverseWord += words[i];
    }

    return reverseWord;
}

console.log(balikKata("Kasur Rusak"));
console.log("\r\n");

/**************************
 * Tugas Array
 * Soal 6
 **************************/

function dataHandling2(input) {
    input.splice(1, 2, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung');
    input.splice(4, 1, 'Pria', 'SMA International Metro');

    console.log("Test Case 1 : ", input);

    birthdate = input[3].split('/');

    let month;
    switch (birthdate[1]) {
        case '01':
            month = 'Januari';
            break;
        case '02':
            month = 'Februari';
            break;
        case '03':
            month = 'Maret';
            break;
        case '04':
            month = 'April';
            break;
        case '05':
            month = 'Mei';
            break;
        case '06':
            month = 'Juni';
            break;
        case '07':
            month = 'Juli';
            break;
        case '08':
            month = 'Agustus';
            break;
        case '09':
            month = 'September';
            break;
        case '10':
            month = 'Oktober';
            break;
        case '11':
            month = 'November';
            break;
        case '12':
            month = 'Desember';
            break;
    }

    console.log("Test Case 2 : ", month);

    let birthdateSorted = input[3].split('/');;
    birthdateSorted.sort(function (v1, v2) {
        return parseInt(v1) < parseInt(v2);
    })

    console.log("Test Case 3 : ", birthdateSorted);

    birthdate[1] = month;
    newbirthdate = birthdate.join('-');

    console.log("Test Case 4 : ", newbirthdate);
    console.log("Test Case 5 : ", input[1].slice(0,14));
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input);
