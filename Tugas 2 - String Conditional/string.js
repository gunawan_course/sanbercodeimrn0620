/**************************
 * Soal 1
 * Membuat Kalimat
 **************************/

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

// Cara ke -1
console.log(
    word + ' '
    .concat(second + ' ')
    .concat(third + ' ')
    .concat(fourth + ' ')
    .concat(fifth + ' ')
    .concat(sixth + ' ')
    .concat(seventh + ' ')
);

// alternatif cara yang lain..

// Cara ke-2
console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`)

// Cara ke-3
var strlist = [word, second, third, fourth, fifth, sixth, seventh]
console.log(strlist.join(' '))

// Cara ke-4
console.log(word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh)
console.log("\r\n")

/**************************
 * Soal 2
 * Mengurai Kalimat
 **************************/

var sentence = "I am going to be React Native Developer"; 

var test = sentence.match(/([a-zA-Z]{1,})/gi);

var firstWord = test[0]; 
var secondWord = test[1]; 
var thirdWord = test[2]; 
var fourthWord = test[3]; 
var fifthWord = test[4]; 
var sixthWord = test[5]; 
var seventhWord = test[6]; 
var eighthWord = test[7]; 

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log("\r\n")

/**************************
 * Soal 3
 * Mengurai Kalimat (substring)
 **************************/

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log("\r\n")

/**************************
 * Soal 4
 * Mengurai Kalimat (substring, length)
 **************************/

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21); 

var firstWordLength = exampleFirstWord3.length
var secondWord3Length = secondWord3.length
var thirdWord3Length = thirdWord3.length
var fourthWord3Length = fourthWord3.length
var fifthWord3Length = fifthWord3.length

console.log(`First Word: ${exampleFirstWord3} with length: ${firstWordLength}`);
console.log(`Second Word: ${secondWord3} with length: ${secondWord3Length}`);
console.log(`Third Word: ${thirdWord3} with length: ${thirdWord3Length}`);
console.log(`Fourth Word: ${fourthWord3} with length: ${fourthWord3Length}`);
console.log(`Fifth Word: ${fifthWord3} with length: ${fifthWord3Length}`);