/**************************
 * Tugas Conditional
 * Soal 1
 * If Else
 **************************/

var nama = '';
var peran = '';

nama = 'John';
peran = '';

if (nama == '' && peran == '') {
    console.log(`Nama harus diisi!`)
} else if (nama != '' && peran == '') {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log(`Selamat datang di Dunia Werewolf, ${name}`);
    console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
} else if (nama == 'Junaidi' && peran == 'Werewolf') {
    console.log(`Selamat datang di Dunia Werewolf, ${name}`);
    console.log(`Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam.`);
}

/**************************
 * Tugas Conditional
 * Soal 2
 * Switch case
 **************************/

 var tanggal;
 var bulan;
 var tahun;

 tanggal = 1;
 bulan = 1;
 tahun = 1945;

 switch (bulan) {
    case 1:
        nama_bulan  = 'Januari';
        break;
    case 2:
        nama_bulan  = 'Februari';
        break;
    case 3:
        nama_bulan  = 'Maret';
        break;
    case 4:
        nama_bulan  = 'April';
        break;
    case 5:
        nama_bulan  = 'Mei';
        break;
    case 6:
        nama_bulan  = 'Juni';
        break;
    case 7:
        nama_bulan  = 'Juli';
        break;
    case 8:
        nama_bulan  = 'Agustus';
        break;
    case 9:
        nama_bulan  = 'September';
        break;
    case 10:
        nama_bulan  = 'Oktober';
        break;
    case 11:
        nama_bulan  = 'November';
        break;
    case 12:
        nama_bulan  = 'Desember';
        break;
 }

 console.log(`${tanggal} ${nama_bulan} ${tahun}`);