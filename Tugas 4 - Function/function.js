/**
 * Nama : Gunawan
 * Email : gunawan.prasetyo@protonmail.com
 * Tugas 4
 */

/**************************
 * Tugas Function
 * Soal 1
 **************************/

 function teriak() {
     return "Halo sanbers!";
 }

 console.log(teriak());

 /**************************
 * Tugas Function
 * Soal 2
 **************************/

function kalikan(num1, num2) {
    return num1 * num2;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

/**************************
 * Tugas Function
 * Soal 3
 **************************/

 function introduce(name, age, address, hobby) {
     return `Nama saya ${name}, umur saya ${age}, alamat saya di ${address}, dan saya punya hobby ${hobby}`;
 }

var name = "John Doe"
var age = 48
var address = "Wonosobo"
var hobby = "Bercocok tanam"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)