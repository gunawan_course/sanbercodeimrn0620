/**
 * Nama : Gunawan
 * Email : gunawan.prasetyo@protonmail.com
 * Tugas 3
 */

/**************************
 * Tugas Looping
 * Soal 1
 * While
 **************************/
console.log("\r\nLOOPING PERTAMA \r\n");
let i = 2;
while (i < 20) {
    if (i%2==0) {
        console.log(`${i} - I Love Coding`);
    }
    i++;
}

console.log("\r\nLOOPING KEDUA \r\n");
i = 19;
while (i >= 2) {
    if (i%2==0) {
        console.log(`${i} - I will become a mobile developer`);
    }
    i--;
}

/**************************
 * Tugas Looping
 * Soal 2
 * For
 **************************/
console.log("\r\nOUTPUT \r\n");

let kalimat = '';
for (i = 1; i <= 20; i++) {
    if (i%3 == 0 && i%2 == 1) {
        kalimat = 'I Love Coding';
    } else if (i%2 == 1) {
        kalimat = 'Santai';
    } else if (i%2 == 0) {
        kalimat = 'Berkualitas';
    }

    console.log(`${i} - ${kalimat}`);
}

/**************************
 * Tugas Looping
 * Soal 3
 * Persegi Panjang
 **************************/
console.log("\r\nPERSEGI PANJANG \r\n");

let width = 8;
let height = 4;
let block = '';
for (i = 0; i < height; i++) {
    for (k = 0; k < width; k++) {
        block += '#';
    }

    block += "\r\n";
}

console.log(block);

/**************************
 * Tugas Looping
 * Soal 4
 * Tangga
 **************************/
console.log("\r\nTANGGA \r\n");

height = 7;
block = '';
for (i = 0; i < height; i++) {
    for (k = 0; k <= i; k++) {
        block += '#';
    }

    block += "\r\n";
}

console.log(block);

/**************************
 * Tugas Looping
 * Soal 5
 * Papan Catur
 **************************/
console.log("\r\nPAPAN CATUR \r\n");

width = 8;
height = 8;
block = '';
for (i = 0; i < height; i++) {
    for (k = 0; k < width; k++) {
        if (i%2 == 0) {
            if (k%2 == 0) {
                block += ' ';
            } else {
                block += '#';
            }
        } else {
            if (k%2 == 0) {
                block += '#';
            } else {
                block += ' ';
            }
        }  
    }

    block += "\r\n";
}

console.log(block);