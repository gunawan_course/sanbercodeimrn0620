var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 12000}, 
    {name: 'Fidas', timeSpent: 4000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let time = 10000;
books.map(book => {
    readBooksPromise(time, book).then(function (sisaWaktu) {
        time = sisaWaktu;
    }).catch(function (error) {
        console.log(`saya butuh waktu ${error.sisaWaktu} ms lagi nih untuk menyelesaikan ${book.name}`);
    });
});