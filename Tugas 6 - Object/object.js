function arrayToObject(data) {
    const fieldName = ['firstName', 'lastName', 'gender', 'age'];
    const obj = [];
    let keyName;

    let now = new Date();
    let thisYear = now.getFullYear();
    
    for (let i = 0; i < data.length; i++) {
        keyName = `${data[i][0]} ${data[i][1]}`;
        obj[ keyName ] = data[i].reduce((k, v, i) => {
            if (fieldName[i] == 'age') {
                k[ fieldName[i] ] = thisYear - v;
            } else {
                k[ fieldName[i] ] = v;
            }
    
            return k;
        }, {});
    }
    
    return obj;
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];

console.log (arrayToObject(people));